package ru.t1.kravtsov.tm.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.kravtsov.tm.api.component.IBootstrap;
import ru.t1.kravtsov.tm.api.repository.ICommandRepository;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.repository.IUserRepository;
import ru.t1.kravtsov.tm.api.service.*;
import ru.t1.kravtsov.tm.command.AbstractCommand;
import ru.t1.kravtsov.tm.command.project.*;
import ru.t1.kravtsov.tm.command.system.*;
import ru.t1.kravtsov.tm.command.task.*;
import ru.t1.kravtsov.tm.command.user.*;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.kravtsov.tm.exception.system.CommandNotSupportedException;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.model.User;
import ru.t1.kravtsov.tm.repository.CommandRepository;
import ru.t1.kravtsov.tm.repository.ProjectRepository;
import ru.t1.kravtsov.tm.repository.TaskRepository;
import ru.t1.kravtsov.tm.repository.UserRepository;
import ru.t1.kravtsov.tm.service.*;
import ru.t1.kravtsov.tm.util.SystemUtil;
import ru.t1.kravtsov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IBootstrap, IServiceLocator {

    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IPropertyService propertyService = new PropertyService();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    private final IAuthService authService = new AuthService(userService, propertyService);

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectDisplayByIdCommand());
        registry(new ProjectDisplayByIndexCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskDisplayByIdCommand());
        registry(new TaskDisplayByIndexCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
    }

    @Override
    public void run(String[] args) {
        initPID();
        initDemoData();
        initLogger();
        if (parseArguments(args)) exit();
        parseCommands();
    }

    private void initPID() {
        final String filename = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        userService.create("test", "test", "test@test.ru");
        final User user = userService.create("user", "user", "user@user.ru");
        final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(user.getId(), new Project("BETA PROJECT", Status.NOT_STARTED));
        projectService.add(user.getId(), new Project("ALPHA PROJECT", Status.IN_PROGRESS));
        projectService.add(admin.getId(), new Project("GAMMA PROJECT", Status.COMPLETED));

        taskService.add(user.getId(), new Task("DELTA TASK"));
        taskService.add(admin.getId(), new Task("EPSILON TASK"));
    }

    private boolean parseArguments(final String[] args) {
        if (args == null || args.length == 0) return false;

        final String arg = args[0];
        parseArgument(arg);
        return true;
    }

    private void parseCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                parseCommand(command);
                LOGGER_COMMANDS.info(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                LOGGER_LIFECYCLE.error(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByArgument(arg);
        if (command == null) throw new ArgumentNotSupportedException(arg);
        command.execute();
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void exit() {
        System.exit(0);
    }

    private void initLogger() {
        LOGGER_LIFECYCLE.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                LOGGER_LIFECYCLE.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
