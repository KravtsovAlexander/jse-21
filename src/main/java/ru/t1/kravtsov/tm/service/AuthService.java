package ru.t1.kravtsov.tm.service;

import ru.t1.kravtsov.tm.api.service.IAuthService;
import ru.t1.kravtsov.tm.api.service.IPropertyService;
import ru.t1.kravtsov.tm.api.service.IUserService;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.exception.field.LoginEmptyException;
import ru.t1.kravtsov.tm.exception.field.PasswordEmptyException;
import ru.t1.kravtsov.tm.exception.system.AccessDeniedException;
import ru.t1.kravtsov.tm.exception.system.AuthException;
import ru.t1.kravtsov.tm.exception.system.PermissionException;
import ru.t1.kravtsov.tm.model.User;
import ru.t1.kravtsov.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private final IPropertyService propertyService;

    private String userId;

    public AuthService(final IUserService userService, final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public User registry(String login, String password, String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null || user.isLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(propertyService, password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AuthException();
        final User user = userService.findOneById(userId);
        if (user == null) throw new AuthException();
        return user;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
