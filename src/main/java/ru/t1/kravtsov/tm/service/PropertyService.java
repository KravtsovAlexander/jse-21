package ru.t1.kravtsov.tm.service;

import com.jcabi.manifests.Manifests;
import ru.t1.kravtsov.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    public static final String GIT_BRANCH = "gitBranch";

    public static final String GIT_COMMIT_ID = "gitCommitId";

    public static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    public static final String GIT_COMMIT_TIME = "gitCommitTime";

    public static final String GIT_COMMITTER_NAME = "gitCommitterName";

    public static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";

    public static final String APPLICATION_NAME_KEY = "application.name";

    public static final String APPLICATION_NAME_DEFAULT = "tm";

    public static final String APPLICATION_FILE_NAME_KEY = "application.config";

    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    public static final String AUTHOR_EMAIL_KEY = "email";

    public static final String AUTHOR_NAME_KEY = "developer";

    public static final String PASSWORD_ITERATION_DEFAULT = "7438";

    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    public static final String PASSWORD_SECRET_DEFAULT = "82392";

    public static final String PASSWORD_SECRET_KEY = "password.secret";

    public static final String EMPTY_VALUE = "--//--";

    private final Properties properties = new Properties();

    public PropertyService() {
        if (externalConfigExists()) {
            loadExternalConfig(properties);
        } else {
            loadInternalConfig(properties);
        }
    }

    private void loadInternalConfig(final Properties properties) {
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream(APPLICATION_FILE_NAME_DEFAULT);
        if (inputStream == null) return;
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void loadExternalConfig(final Properties properties) {
        try {
            final String name = getApplicationConfig();
            final File file = new File(name);
            final InputStream inputStream = new FileInputStream(file);
            properties.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean externalConfigExists() {
        final String name = getApplicationConfig();
        final File file = new File(name);
        return file.exists();
    }

    @Override
    public String getApplicationName() {
        return getStringValue(APPLICATION_NAME_KEY, APPLICATION_NAME_DEFAULT);
    }

    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    private String getStringValue(final String key, final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    private String getStringValue(final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    private Integer getIntegerValue(final String key, final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    private String getEnvKey(final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    private String read(final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @Override
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @Override
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @Override
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @Override
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @Override
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL);
    }

}
