package ru.t1.kravtsov.tm.api.service;

public interface ISaltProvider {

    Integer getPasswordIteration();

    String getPasswordSecret();

}
