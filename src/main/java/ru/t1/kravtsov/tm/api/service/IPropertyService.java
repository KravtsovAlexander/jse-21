package ru.t1.kravtsov.tm.api.service;

public interface IPropertyService extends ISaltProvider {

    String getApplicationName();

    String getApplicationConfig();

    String getApplicationVersion();

    String getAuthorEmail();

    String getAuthorName();

    String getGitBranch();

    String getGitCommitId();

    String getGitCommitMessage();

    String getGitCommitTime();

    String getGitCommitterName();

    String getGitCommitterEmail();

}
