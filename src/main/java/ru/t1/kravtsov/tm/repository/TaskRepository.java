package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository() {
    }

    @Override
    public List<Task> removeByName(final String userId, final String name) {
        return findAll(userId)
                .stream()
                .filter(m -> name.equals(m.getName()))
                .peek(m -> models.remove(m.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return findAll(userId)
                .stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

}
