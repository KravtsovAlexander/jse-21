package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.IRepository;
import ru.t1.kravtsov.tm.model.AbstractModel;

import java.util.*;
import java.util.function.Predicate;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected Predicate<M> filterById(final String id) {
        return m -> id.equals(m.getId());
    }

    protected final Map<String, M> models = new LinkedHashMap<>();

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public void deleteAll() {
        models.clear();
    }

    @Override
    public void deleteAll(final List<M> models) {
        models.stream()
                .map(AbstractModel::getId)
                .forEach(this.models::remove);
    }

    @Override
    public List<M> findAll() {
        return new ArrayList<>(models.values());
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(models.values());
        result.sort(comparator);
        return result;
    }

    @Override
    public M add(final M model) {
        models.put(model.getId(), model);
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public M findOneById(final String id) {
        return models.get(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.values().stream().skip(index).findFirst().orElse(null);
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public M remove(final M model) {
        if (model == null) return null;
        models.remove(model.getId());
        return model;
    }

    @Override
    public M removeById(final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

}
