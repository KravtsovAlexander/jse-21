package ru.t1.kravtsov.tm.enumerated;

public enum Role {

    USUAL("Usual user"),

    ADMIN("Administrator");

    private final String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
