package ru.t1.kravtsov.tm.command.project;

import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Remove project by name.";

    public static final String NAME = "project-remove-by-name";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final List<Project> projectsForDeletion = new ArrayList<>();
        for (final Project project : getProjectService().findAll(getUserId())) {
            if (project.getName() == null) continue;
            if (project.getName().equals(name)) {
                projectsForDeletion.add(project);
            }
        }
        getProjectTaskService().removeProjects(getUserId(), projectsForDeletion);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
