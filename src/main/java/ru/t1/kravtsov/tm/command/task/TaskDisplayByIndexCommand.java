package ru.t1.kravtsov.tm.command.task;

import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskDisplayByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Display task by index.";

    public static final String NAME = "task-display-by-index";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer input = TerminalUtil.nextNumber();
        final Integer index = input - 1;
        final Task task = getTaskService().findOneByIndex(getUserId(), index);
        displayTask(task);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
