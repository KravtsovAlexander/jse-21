package ru.t1.kravtsov.tm.command.system;

import ru.t1.kravtsov.tm.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Display developer info.";

    public static final String NAME = "about";

    @Override
    public void execute() {
        final IPropertyService propertyService = getPropertyService();
        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + propertyService.getApplicationName());
        System.out.println();

        System.out.println("[DEVELOPER]");
        System.out.println("NAME: " + propertyService.getAuthorName());
        System.out.println("EMAIL: " + propertyService.getAuthorEmail());
        System.out.println();

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + propertyService.getGitBranch());
        System.out.println("COMMIT ID: " + propertyService.getGitCommitId());
        System.out.println("COMMIT MESSAGE: " + propertyService.getGitCommitMessage());
        System.out.println("COMMIT TIME: " + propertyService.getGitCommitTime());
        System.out.println("COMMITTER NAME: " + propertyService.getGitCommitterName());
        System.out.println("COMMITTER EMAIL: " + propertyService.getGitCommitterEmail());
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
