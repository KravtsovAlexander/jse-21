package ru.t1.kravtsov.tm.command.task;

import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Remove task by name.";

    public static final String NAME = "task-remove-by-name";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        getTaskService().removeByName(getUserId(), name);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
