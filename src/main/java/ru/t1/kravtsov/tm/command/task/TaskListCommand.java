package ru.t1.kravtsov.tm.command.task;

import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Show task list.";

    public static final String NAME = "task-list";

    @Override
    public void execute() {
        System.out.println("[TASKS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(getUserId(), sort);
        renderTasks(tasks);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
