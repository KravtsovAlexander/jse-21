package ru.t1.kravtsov.tm.command.task;

import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Start task by id.";

    public static final String NAME = "task-start-by-id";

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(getUserId(), id, Status.IN_PROGRESS);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
