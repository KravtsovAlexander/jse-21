package ru.t1.kravtsov.tm.command.project;

import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Complete project by index.";

    public static final String NAME = "project-complete-by-index";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(getUserId(), index, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
