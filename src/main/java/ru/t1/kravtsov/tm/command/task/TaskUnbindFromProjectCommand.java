package ru.t1.kravtsov.tm.command.task;

import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Unbind task from project.";

    public static final String NAME = "task-unbind-from-project";

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectID = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskID = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(getUserId(), projectID, taskID);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
