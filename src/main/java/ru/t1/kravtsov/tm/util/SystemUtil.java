package ru.t1.kravtsov.tm.util;

import java.lang.management.ManagementFactory;

public interface SystemUtil {

    static long getPID() {
        final String processName = ManagementFactory.getRuntimeMXBean().getName();
        if (processName == null || processName.isEmpty()) {
            return 0;
        }
        try {
            return Long.parseLong(processName.split("@")[0]);
        } catch (Exception e) {
            return 0;
        }
    }

}
